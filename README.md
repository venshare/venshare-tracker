# venshare-tracker

A bittorrent tracker service 

## Install 

```bash
$ git clone git@gitlab.com:venshare/venshare-tracker.git
$ cd venshare-tracker 
$ npm install 
```

## Run 

```bash
$ node server.js
```

## Attribution
Forked from the inactive but awesome node-torrent-tracker. 
http://github.com/WizKid/node-bittorrent-tracker
By Emil Hesslow
